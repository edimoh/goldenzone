/*
 * Created by Daniel Marell 13-12-22 23:23
 */
package com.caglabs.goldenzone.reqtestclient;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.junit.Test;

//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.RuntimeDelegate;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.URI;

import nu.xom.ParsingException;
import java.util.List;

import static org.junit.Assert.assertEquals;

import com.sun.jersey.api.client.Client;
//import com.sun.net.httpserver.HttpHandler;
//import com.sun.net.httpserver.HttpServer;
//import nu.xom.ParsingException;
//import org.junit.Test;
//
//import javax.ws.rs.core.UriBuilder;
//import javax.ws.rs.ext.RuntimeDelegate;
//import java.io.IOException;
//import java.net.InetSocketAddress;
//import java.net.URI;
//import java.util.List;
//
//import static org.hamcrest.core.Is.is;
//import static org.junit.Assert.assertThat;

public class ReQtestClientIT {

    @Test
    public void shouldGetTestCases() throws IOException {

        URI uri = UriBuilder.fromUri("http://localhost/").port(8282).build();

        // Create an HTTP server listening at port 8282
        HttpServer server = HttpServer.create(new InetSocketAddress(uri.getPort()), 0);
        // Create a handler wrapping the JAX-RS application
        HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(new ApplicationConfig(), HttpHandler.class);
        // Map JAX-RS handler to the server root
        server.createContext(uri.getPath(), handler);
        // Start the server
        server.start();

        ReQtestClient client = new ReQtestClient("http://localhost:8282/", "10211", "24313", "cag2013");
        try {
            List<TestCase> testCases = client.getTestCases();
            for (TestCase tc : testCases) {
                System.out.println("id=" + tc.getId() + ",Rubrik=" + tc.getFieldValue("Rubrik") + ",Status=" + tc.getFieldValue("Status"));
            }
        } catch (ParsingException e) {
            e.printStackTrace();
        }

//        Client client = ClientFactory.newClient();
//
//        // Valid URIs
//        assertThat(client.target("http://localhost:8282/customer/agoncal").request().get().getStatus(), is(200));

        // Stop HTTP server
        server.stop(0);
    }
}

