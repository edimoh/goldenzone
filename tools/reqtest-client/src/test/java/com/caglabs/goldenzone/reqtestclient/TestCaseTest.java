/*
 * Created by Daniel Marell 13-12-15 17:18
 */
package com.caglabs.goldenzone.reqtestclient;

import com.caglabs.goldenzone.common.TestUtil;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class    TestCaseTest {
    @Test
    public void test() throws Exception {
        /*
        testcases.xml:
            <id>458788</id>
            <archived>false</archived>
            <attachmentCount>0</attachmentCount>
            <changedDate i:nil="true"/>
            <commentCount>0</commentCount>
            <createdBy>Anders Englesson</createdBy>
            <createdByUserId>24119</createdByUserId>
            <createdDate>2013-12-02 10:33:17</createdDate>
            <customId>1</customId>
            <fieldValues>
                <fieldValue>
                    <fieldId>181083</fieldId>
                    <fieldName>Granskare</fieldName>
                    <value i:nil="true"/>
                    <valueId i:nil="true"/>
                </fieldValue>
            ...
            <links>
                <link>
                    <href>
                        https://secure.reqtest.com/api/v1/projects/10211/testcases/458788
                    </href>
                    <name>self</name>
                </link>
         */
//        TestCases testCases = TestCases.parseTestCases(
//                IOUtils.toString(getClass().getClassLoader().getResourceAsStream("testcases.xml"), "UTF-8"));
//        assertThat(testCases.getTestCases().size(), is(1));
//        TestCase tc = testCases.getTestCases().get(0);
//        assertThat(tc.getId(), is(458788));
//        assertThat(tc.isArchived(), is(false));
//        assertThat(tc.getAttachmentCount(), is(0));
//        assertThat(tc.getChangedDate(), nullValue());
//        assertThat(tc.getCommentCount(), is(0));
//        assertThat(tc.getCreatedBy(), is("Anders Englesson"));
//        assertThat(tc.getCreatedByUserId(), is(24119));
//        assertThat(tc.getCreatedDate(), is(TestUtil.createDate("2013-12-02 10:33:17")));
//        assertThat(tc.getCustomId(), is(1));
//        assertThat(tc.getFields().size(), is(12));
//        TestCaseField f = tc.getFields().get(0);
//        assertThat(f.getFieldId(), is(181083));
//        assertThat(f.getFieldName(), is("Granskare"));
//        assertThat(f.getValue(), nullValue());
//        assertThat(f.getValueId(), nullValue());
    }
}
