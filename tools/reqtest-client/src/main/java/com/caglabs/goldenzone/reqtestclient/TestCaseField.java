/*
 * Created by Daniel Marell 13-12-15 16:30
 */
package com.caglabs.goldenzone.reqtestclient;

import nu.xom.Element;
import nu.xom.ParsingException;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Represents a "fieldValue" element in a ReQtest test case.
 */
//@XmlRootElement(namespace = "http://reqtest.com/API/V1/")
public class TestCaseField {
    private int fieldId;
    private String fieldName;
    private String value;
    private Integer valueId;

    public TestCaseField() {
    }

    public TestCaseField(int fieldId, String fieldName, String value, Integer valueId) {
        this.fieldId = fieldId;
        this.fieldName = fieldName;
        this.value = value;
        this.valueId = valueId;
    }

    public int getFieldId() {
        return fieldId;
    }

    public void setFieldId(int fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getValueId() {
        return valueId;
    }

    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

//    public static TestCaseField parseFieldValue(Element element) throws ParsingException {
//        int fieldId = ParseHelper.parseIntElement(element, "fieldId", false);
//        String fieldName = ParseHelper.parseStringElement(element, "fieldName", false);
//        String value = ParseHelper.parseStringElement(element, "value", true);
//        Integer valueId = ParseHelper.parseIntElement(element, "valueId", true);
//        return new TestCaseField(fieldId, fieldName, value, valueId);
//    }
}