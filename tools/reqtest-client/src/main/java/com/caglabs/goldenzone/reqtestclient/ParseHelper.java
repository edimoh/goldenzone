/*
 * Created by Daniel Marell 13-12-15 20:06
 */
package com.caglabs.goldenzone.reqtestclient;

import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.ParsingException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParseHelper {
    public static final String NS =  "http://reqtest.com/API/V1/";
    public static final String NS_I =  "http://www.w3.org/2001/XMLSchema-instance";

    public static String parseStringElement(Element parent, String elementName, boolean optional) throws ParsingException {
        Element e = parent.getFirstChildElement(elementName, NS);
        if (e == null) {
            if (optional) {
                return null;
            }
            throw new ParsingException("Missing mandatory element " + elementName);
        } else {
            Attribute nil = e.getAttribute("nil", NS_I);
            if (nil != null) {
                if ("true".equals(nil.getValue())) {
                    if (optional) {
                        return null;
                    }
                    throw new ParsingException("Missing mandatory element " + elementName);
                }
            }
        }
        return e.getValue();
    }

    public static Boolean parseBooleanElement(Element parent, String elementName, boolean optional) throws ParsingException {
        String value = parseStringElement(parent, elementName, optional);
        if (value == null) {
            return null;
        }
        try {
            return Boolean.parseBoolean(value);
        } catch (NumberFormatException e1) {
            throw new ParsingException("Not a boolean: Element " + elementName);
        }
    }

    public static Integer parseIntElement(Element parent, String elementName, boolean optional) throws ParsingException {
        String value = parseStringElement(parent, elementName, optional);
        if (value == null) {
            return null;
        }
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e1) {
            throw new ParsingException("Not an integer: Element " + elementName);
        }
    }

    public static Date parseDateElement(Element parent, String elementName, boolean optional) throws ParsingException {
        String value = parseStringElement(parent, elementName, optional);
        if (value == null) {
            return null;
        }
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return df.parse(value);
        } catch (ParseException e) {
            throw new ParsingException("Not a date: Element " + elementName + ":" + value);
        }
    }

}
