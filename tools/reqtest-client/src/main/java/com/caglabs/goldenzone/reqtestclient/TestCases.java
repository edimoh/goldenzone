/*
 * Created by Daniel Marell 13-12-15 22:33
 */
package com.caglabs.goldenzone.reqtestclient;

import nu.xom.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses an XML response from ReQtest REST api call GET "testcases".
 */
@XmlRootElement(namespace = "http://reqtest.com/API/V1/")
//@XmlSeeAlso(TestCase.class)
public class TestCases {
    private long projectId;
    private List<TestCase> testCases;

    public TestCases() {}

    public TestCases(long projectId, List<TestCase> testCases) {
        this.projectId = projectId;
        this.testCases = testCases;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

//    @XmlElement(name = "testCase")
    public List<TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TestCase> testCases) {
        this.testCases = testCases;
    }

//    public static TestCases parseTestCases(Element element) throws ParsingException {
//        int projectId = ParseHelper.parseIntElement(element, "projectId", false);
//        Element testCasesElem = element.getFirstChildElement("testCases", ParseHelper.NS);
//        Elements testCaseElems = testCasesElem.getChildElements("testCase", ParseHelper.NS);
//        List<TestCase> testCases = new ArrayList<>();
//        for (int i = 0; i < testCaseElems.size(); ++i) {
//            testCases.add(TestCase.parseTestCase(testCaseElems.get(i)));
//        }
//        return new TestCases(projectId, testCases);
//    }
//
//    public static TestCases parseTestCases(String xml) throws ParsingException {
//        try {
//            Builder parser = new Builder();
//            Document doc = parser.build(new StringReader(xml));
//            Element testCaseCollectionElem = doc.getRootElement();
//            testCaseCollectionElem.setNamespaceURI(ParseHelper.NS);
//            return TestCases.parseTestCases(testCaseCollectionElem);
//        } catch (IOException e) {
//            throw new ParsingException("Failed to read xml", e);
//        }
//    }
}
