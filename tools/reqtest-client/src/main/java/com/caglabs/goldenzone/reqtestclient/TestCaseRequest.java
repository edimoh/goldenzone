/*
 * Created by Daniel Marell 13-12-21 23:37
 */
package com.caglabs.goldenzone.reqtestclient;

import java.util.ArrayList;
import java.util.List;

public class TestCaseRequest {
    private int id;
    private List<TestCaseField> fields = new ArrayList<>();

    public TestCaseRequest(int id, List<TestCaseField> fields) {
        this.id = id;
        this.fields = fields;
    }

    public int getId() {
        return id;
    }

    public List<TestCaseField> getFields() {
        return fields;
    }
}
