/*
 * Created by Daniel Marell 13-11-26 8:41 AM
 */
package com.caglabs.goldenzone.reqtestclient;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import nu.xom.ParsingException;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;

public class ReQtestClient {
    public static final String API_URL = "https://secure.reqtest.com/api/v1/projects/";
    private String endpoint;
    private Client client;
    private String projectId;

    public ReQtestClient(String endpoint, String projectId, String username, String password) {
        this.endpoint = endpoint;
        this.projectId = projectId;
        client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter(username, password));
    }

//    public void addUser(String username, String displayName) {
//        WebResource webResource = client.resource(API_URL + "users/" + username);
//        ClientResponse response = webResource.type("text/plain").put(ClientResponse.class, displayName);
//        if (response.getStatus() != 204) {
//            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
//        }
//    }
//
//    public void removeUser(String username) {
//        WebResource webResource = client.resource(API_URL + "users/" + username);
//        ClientResponse response = webResource.type("text/plain").delete(ClientResponse.class);
//        if (response.getStatus() != 204) {
//            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
//        }
//    }
//

    public void updateTestCase(int testCaseId) {
        /*
        <testCase xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://reqtest.com/API/V1/">
          <id>72066</id>
          <fieldValues>
            <fieldValue>
              <fieldId>31043</fieldId>
              <value>Title text</value>
            </fieldValue>
            <fieldValue>
              <fieldId>31042</fieldId>
              <valueId>17453</valueId>
            </fieldValue>
          </fieldValues>
        </testCase>
        */
        WebResource webResource = client.resource(endpoint + projectId + "/testcases/" + testCaseId);
        TestCaseRequest request = new TestCaseRequest(458788, Arrays.asList(new TestCaseField(181079, "Status", "Test 1", 0)));
        ClientResponse response = webResource.type(MediaType.APPLICATION_XML).post(ClientResponse.class, request);
        String reply = response.getEntity(String.class);
        System.out.println("reply=" + reply);
    }

    public List<TestCase> getTestCases() throws ParsingException {
        WebResource webResource = client.resource(endpoint + projectId + "/testcases");
        ClientResponse response = webResource.type(MediaType.APPLICATION_XML).get(ClientResponse.class);
        TestCases reply = response.getEntity(TestCases.class);
        return reply.getTestCases();
//        String reply = response.getEntity(String.class);
//        return TestCases.parseTestCases(reply).getTestCases();
    }

    public static void main(String[] args) {
        ReQtestClient client = new ReQtestClient(API_URL, "10211", "24313", "cag2013");
        try {
            List<TestCase> testCases = client.getTestCases();
            for (TestCase tc : testCases) {
                System.out.println("id=" + tc.getId() + ",Rubrik=" + tc.getFieldValue("Rubrik") + ",Status=" + tc.getFieldValue("Status"));
            }
        } catch (ParsingException e) {
            e.printStackTrace();
        }
    }
}
