/*
 * Created by Daniel Marell 13-12-15 23:24
 */
package com.caglabs.goldenzone.reqtestclient;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

public class GitClient {
    public static void main(String[] args) throws Exception {
        Collection<Ref> refs = Git.lsRemoteRepository()
                //.setHeads(true)
                .setTags(true)
                .setRemote("https://bitbucket.org/cagcontactor/goldenzone.git")
                .call();

        for (Ref ref : refs) {
            System.out.println("Ref: " + ref);
        }


//        FileRepositoryBuilder builder = new FileRepositoryBuilder();
////        File repoDir = new File("/Volumes/ssd1t/swc/goldenzone");
//        File repoDir = new File("https://bitbucket.org/cagcontactor/goldenzone.git");
//        Repository repository = builder.setGitDir(repoDir)
//                .readEnvironment() // scan environment GIT_* variables
//                .setGitDir(repoDir)
//                //.setGitDir(new File(repoDir, ".git"))
//                .build();
//
//        System.out.println("Having repository: " + repository.getDirectory());
//
//        try {
//            List<Ref> call = new Git(repository).tagList().call();
//            for (Ref ref : call) {
//                System.out.println("Tag: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
//            }
//        } catch (GitAPIException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            Iterable<RevCommit> logs = new Git(repository).log()
//                    .all()
//                    .call();
//            int count = 0;
//            for (RevCommit rev : logs) {
//                System.out.println("Commit: " + rev /* + ", name: " + rev.getName() + ", id: " + rev.getId().getName() */);
//                count++;
//            }
//            System.out.println("Had " + count + " commits overall on current branch");
//        } catch (GitAPIException e) {
//            e.printStackTrace();
//        }


        //repository.close();
    }

}
