/*
 * Created by Daniel Marell 13-10-27 8:44 PM
 */
package com.caglabs.goldenzone.helloclient;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

/**
 * Helper class for getting a handle to the Hello web service.
 */
public class HelloServiceLocator {
    private String endpoint;
    private String username;
    private String password;

    public HelloServiceLocator() {
        endpoint = "http://localhost:8082/HelloBeanService/HelloBean";
        username = "admin";
        password = "";
    }

    public HelloServiceLocator(String endpoint, String username, String password) {
        this.endpoint = endpoint;
        this.username = username;
        this.password = password;
    }

    public HelloBean getService() {
        HelloBeanService service = new HelloBeanService(
                null,
                new QName("http://impl.helloservice.goldenzone.caglabs.com/", "HelloBeanService"));
        HelloBean ws = service.getHelloBeanPort();
        // Add username and password for container authentication
        BindingProvider bp = (BindingProvider) ws;
        bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, username);
        bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, password);
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
        return ws;
    }
}