/*
 * Created by Daniel Marell 13-11-26 8:41 AM
 */
package com.caglabs.goldenzone.helloclient;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

public class HelloRestClient {
    private static final String API_URL = "http://localhost:8082/hello-rest/";
    private Client client = Client.create();

    public void addUser(String username, String displayName) {
        WebResource webResource = client.resource(API_URL + "users/" + username);
        ClientResponse response = webResource.type("text/plain").put(ClientResponse.class, displayName);
        if (response.getStatus() != 204) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
        }
    }

    public void removeUser(String username) {
        WebResource webResource = client.resource(API_URL + "users/" + username);
        ClientResponse response = webResource.type("text/plain").delete(ClientResponse.class);
        if (response.getStatus() != 204) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
        }
    }

    public List<String> getUserNames() {
        WebResource webResource = client.resource(API_URL + "users");
        ClientResponse response = webResource.queryParams(new MultivaluedMapImpl()).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        String jsonReply = response.getEntity(String.class);
        System.out.println("result=" + jsonReply);

        List<String> result = new ArrayList<>();
        try {
            JSONArray array = (JSONArray) new JSONParser().parse(jsonReply);
            for (Object obj : array) {
                result.add((String) obj);
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException("Failed to parse json:" + e.getMessage() + ",json=" + jsonReply);
        }
        return result;
    }

    public String getDisplayName(String username) {
        WebResource webResource = client.resource(API_URL + "users/" + username);
        ClientResponse response = webResource.queryParams(new MultivaluedMapImpl()).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200) {
            return null;
        }
        try {
            JSONObject obj = (JSONObject) new JSONParser().parse(response.getEntity(String.class));
            return (String) obj.get("displayName");
        } catch (ParseException e) {
            return null;
        }
    }
}
