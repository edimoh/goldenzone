package com.caglabs.goldenzone.infomodel.dbloader;


import org.springframework.test.util.ReflectionTestUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.logging.Logger;

public abstract class AbstractDBLoaderBase {
    public static class DBLoadException extends Exception {

        public DBLoadException(Exception e) {
            super(e);
        }
    }

    private static Logger logger = Logger.getLogger(AbstractDBLoaderBase.class.getName());

    private EntityManagerFactory emFactory;

    private EntityManager entityManager;

    private void setupEntityManager() throws ClassNotFoundException, SQLException {
        logger.info("Setting PU for DB loading");
        emFactory = Persistence.createEntityManagerFactory("GoldenzonePU-dbloader-IT");
        entityManager = emFactory.createEntityManager();
    }

    private void tearDownEntityManager() {
        logger.info("Shutting PU down");
        if (entityManager != null) {
            entityManager.close();
        }
        if (emFactory != null) {
            emFactory.close();
        }
    }

    protected void injectEntityManager(Object o) {
        for (Field field : o.getClass().getDeclaredFields()) {
            if (field.getAnnotation(PersistenceContext.class) != null) {
                field.setAccessible(true);
                ReflectionTestUtils.setField(o, field.getName(), entityManager);
            }
        }
    }

    protected void withinTransaction(Runnable runnable) {
        entityManager.getTransaction().begin();
        try {
            runnable.run();
            entityManager.getTransaction().commit();
        } catch (Throwable t) {
            entityManager.getTransaction().rollback();
        }
    }

    public void execute() throws DBLoadException {
        try {
            setupEntityManager();
            loadDB();
        } catch (ClassNotFoundException | SQLException e) {
            throw new DBLoadException(e);
        } finally {
            tearDownEntityManager();
        }
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected abstract void loadDB();
}
