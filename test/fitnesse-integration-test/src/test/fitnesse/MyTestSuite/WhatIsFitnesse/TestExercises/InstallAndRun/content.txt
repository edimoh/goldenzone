## !contents -R2 -g -p -f -h

git clone ...  or git pull
mvn install

Unpack !-FitNesse-!
{{{
> cd test/fitnesse-integration-test
> mvn uk.co.javahelp.fitnesse:fitnesse-launcher-maven-plugin:1.3.0:set-up -Pint-test
}}}

Run !-FitNesse-!
{{{
> cd test/fitnesse-integration-test
> mvn uk.co.javahelp.fitnesse:fitnesse-launcher-maven-plugin:1.3.0:wiki -Pint-test
}}}

Command line execution of .MyTestSuite
{{{
> cd test/fitnesse-integration-test
> mvn install -Pint-test
}}}

