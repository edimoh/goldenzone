package example;

import com.caglabs.goldenzone.helloservice.client.Hello;
import com.caglabs.goldenzone.infomodel.dbloader.AbstractDBLoaderBase;
import com.caglabs.goldenzone.infomodel.dbloader.DBUtil;
import com.caglabs.goldenzone.infomodel.dbloader.ExampleInfoModelDBLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;

import static junit.framework.Assert.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Test the HelloBean service.
 */
public class ITHello {
    @Before
    public void setup() throws AbstractDBLoaderBase.DBLoadException {
        // Populate DB before each test
        new ExampleInfoModelDBLoader().execute();
    }

    @After
    public void teardown() {
        // Drop DB after each test
        DBUtil.dropDB();
    }

    @Test
    public void shouldSayHello() throws Exception {
        Hello hello = lookupEjbHelloService();
        String greeting = hello.sayHello("kalle0");
        assertThat(greeting, containsString("Hello Kalle"));
    }

    @Test
    public void shouldAddUser() throws Exception {
        Hello hello = lookupEjbHelloService();

        // First verify that user does exist
        try {
            hello.sayHello("newton");
            fail("Expected exception");
        } catch (Hello.HelloException e) {
            // Expected
        }

        // Create user
        hello.addUser("newton", "Isaac Newton");

        // Verify that user now exist
        String greeting = hello.sayHello("newton");
        assertThat(greeting, containsString("Isaac"));
    }

    @Test
    public void shouldRemoveUser() throws Exception {
        Hello hello = lookupEjbHelloService();

        // First verify that user exist
        String greeting = hello.sayHello("kalle0");
        assertThat(greeting, containsString("Kalle"));

        // Remove user
        hello.removeUser("kalle0");

        // Verify that user does not exist
        try {
            hello.sayHello("kalle0");
            fail("Expected exception");
        } catch (Hello.HelloException e) {
            // Expected
        }
    }

    @Test
    public void shouldUpdateUser() throws Exception {
        Hello hello = lookupEjbHelloService();

        // First verify that user exist
        String greeting = hello.getDisplayName("kalle1");
        assertThat(greeting, is("Kalle #1"));

        // Update user
        hello.updateUser("kalle1", "foobar");

        // Verify that user has been updated
        greeting = hello.getDisplayName("kalle1");
        assertThat(greeting, is("foobar"));
    }

    @Test
    public void shouldListUserNames() throws Exception {
        Hello hello = lookupEjbHelloService();

        // First verify that user exist
        List<String> names = hello.getUserNames();
        assertThat(names.size(), is(3));
        assertTrue(names.contains("kalle0"));
        assertTrue(names.contains("kalle1"));
        assertTrue(names.contains("kalle2"));
    }

    protected Hello lookupEjbHelloService() throws NamingException {
        final Context context = new InitialContext();
        String name = "java:global/goldenzone/hello-service/HelloBean";
        System.out.println("Looking up: " + name);
        return (Hello) context.lookup(name);
    }
}
