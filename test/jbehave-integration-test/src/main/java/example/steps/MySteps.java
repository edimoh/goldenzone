package example.steps;

import static org.junit.Assert.assertThat;

import javax.naming.NamingException;

import org.hamcrest.text.StringStartsWith;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.caglabs.goldenzone.helloclient.HelloBean;
import com.caglabs.goldenzone.helloclient.HelloServiceLocator;
import com.caglabs.goldenzone.helloservice.client.Hello;

public class MySteps {
	private static String greeting;

	
	@BeforeStories
	public void setup() throws Exception {
	}

	@Given("that user $user exists in db with display name $display")
	public void setupUser(@Named("user") String user,
			@Named("display") String display)
			throws Exception {
		// Ensure database is populated
		// new ExampleInfoModelDBLoader().execute(); 
		HelloBean hello = new HelloServiceLocator().getService();
		try {
			hello.addUser(user, display);
		} catch (Exception e) {
			hello.updateUser(user, display);
		}
	}

	@AfterScenario
	public void teardown() throws Exception {
		HelloBean hello = new HelloServiceLocator().getService();
		hello.removeUser("kalle");
	}

//	public void after(String user) throws Exception {
//		HelloBean hello = new HelloServiceLocator().getService();
//		hello.removeUser(user);
//
//	}

	@When("$user says {hello|hallo}")
	public void testA(@Named("user") String user) throws Exception,
			Hello.HelloException {
		HelloBean hello = new HelloServiceLocator().getService();
		greeting = hello.sayHello(user);
	}

	@Then("greeting is $greeting")
	public void checkGreeting(@Named("greeting") String g)
			throws NamingException, Hello.HelloException {
		assertThat(greeting, StringStartsWith.startsWith(g));
	}

	public static void main(String[] args) throws Exception {
//		MySteps s = new MySteps();
//		s.setup();
//		s.testA("kalle");
//		s.checkGreeting("Hello Kalle Banan");
//		s.after("kalle");
//        HelloBean hello = new HelloServiceLocator().getService();
//        try {
//            String greeting = hello.sayHello("kalle");
//            System.out.println("Greeting=" + greeting);
//        } catch (HelloException_Exception e) {
//            System.out.println("Caught exception: " + e.getMessage());
//        }
	}
}
