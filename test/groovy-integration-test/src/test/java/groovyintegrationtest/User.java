/*
 * Created by Daniel Marell 13-11-18 8:17 PM
 */
package groovyintegrationtest;

public class User {
    private String name;
    private String displayName;

    User(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    String getName() {
        return name;
    }

    String getDisplayName() {
        return displayName;
    }
}
