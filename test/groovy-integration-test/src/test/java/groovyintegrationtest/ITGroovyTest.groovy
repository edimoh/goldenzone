/*
 * Created by Daniel Marell 13-11-18 7:44 AM
 */
package groovyintegrationtest

import com.caglabs.goldenzone.helloclient.HelloBean
import com.caglabs.goldenzone.helloclient.HelloServiceLocator
import groovy.sql.Sql

class ITGroovyTest extends GroovyTestCase {
    private Sql sql;

    void setUp() {
        sql = Sql.newInstance('jdbc:mysql://localhost:3306/goldenzone_it', 'test', 'test', 'com.mysql.jdbc.Driver')
        sql.execute("delete from goldenzone_user")
    }

    void tearDown() {
        sql.close()
    }

    void testInsertAndSelect() {
        sql.executeInsert("insert into goldenzone_user(id, name, display_name) values (201, 'user1', 'User 1')")
        sql.executeInsert("insert into goldenzone_user(id, name, display_name) values (202, 'user2', 'User 2')")
        sql.eachRow('select * from goldenzone_user') { println "$it.id -- ${it.name} --" }
    }

    void testDbLoad_3_0() {
        new GroovyDbLoader().load(sql)
    }

    void testDbLoad_0_2() {
        new GroovyDbLoader().withNumUsers(0)
                .withUser(new User("daniel", "Daniel Marell"))
                .withUser(new User("anders", "Anders Ekdahl"))
                .load(sql)
    }

    void testDbLoad_10_0() {
        new GroovyDbLoader().withNumUsers(10).load(sql)
    }

    void testAddUser() {
        // Given three users
        new GroovyDbLoader().load(sql)

        // When adding user "foobar"
        HelloBean service = new HelloServiceLocator().getService();
        service.addUser("foobar", "Foo bar")

        // Then number of users shall be 4
        assert service.getNumberOfUsers() == 4
        // And "foobar" shall be found among user names
        assert service.getUserNames().contains("foobar")
    }
}
