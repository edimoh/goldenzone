/*
 * Created by Daniel Marell 13-11-14 8:33 PM
 */
package com.caglabs.goldenzone.hellorest;

import com.caglabs.goldenzone.helloservice.client.Hello;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.logging.Logger;

@Path("users")
public class UserResource {
    private Logger logger = Logger.getLogger(UserResource.class.getName());

    @Path("{name}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(@PathParam("name") String name) throws Hello.HelloException {
        logger.fine("getUser,name=" + name);
        String displayName = lookupHelloService().getDisplayName(name);
        return new User(name, displayName);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getUserNames() {
        logger.fine("getUserNames");
        return lookupHelloService().getUserNames();
    }

    @Path("{name}")
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void setDisplayName(@PathParam("name") String name, String displayName) throws Hello.HelloException {
        logger.fine("setDisplayName,name=" + name + ",displayName=" + displayName);
        Hello hello = lookupHelloService();
        try {
            hello.updateUser(name, displayName);
        } catch (Hello.HelloException e) {
            hello.addUser(name, displayName);
        }
    }

    @Path("{name}")
    @DELETE
    public void removeUser(@PathParam("name") String name) throws Hello.HelloException {
        logger.fine("removeUser,name=" + name);
        lookupHelloService().removeUser(name);
    }

    protected Hello lookupHelloService() {
        try {
            final Context context = new InitialContext();
            String name = "java:global/goldenzone/hello-service/HelloBean";
            return (Hello) context.lookup(name);
        } catch (NamingException e) {
            throw new IllegalArgumentException("ejb lookup failed", e);
        }
    }
}
