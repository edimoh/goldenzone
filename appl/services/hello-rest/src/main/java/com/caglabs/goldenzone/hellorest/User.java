/*
 * Created by Daniel Marell 13-11-14 8:35 PM
 */
package com.caglabs.goldenzone.hellorest;

public class User {
    private String name;
    private String displayName;

    public User() {
    }

    public User(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
