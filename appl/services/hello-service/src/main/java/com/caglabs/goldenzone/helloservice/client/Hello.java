package com.caglabs.goldenzone.helloservice.client;

import java.util.List;

public interface Hello {
    public static class HelloException extends Exception {
        public HelloException(String message) {
            super(message);
        }

        public HelloException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Return a greeting message if username is known.
     *
     * @param username User name
     * @return Greeting message
     * @throws HelloException If username is not a known user
     */
    String sayHello(String username) throws HelloException;

    /**
     * Get display name for user.
     * @param username User name
     * @return User display name
     * @throws HelloException If username is not a known user
     */
    String getDisplayName(String username) throws HelloException;

    /**
     * Create new user.
     *
     * @param username    User name
     * @param displayName Name to display for this user
     * @throws HelloException If a user with this username already exist
     */
    void addUser(String username, String displayName) throws HelloException;

    /**
     * Remove user.
     *
     * @param username User name
     * @throws HelloException If username is not a known user
     */
    void removeUser(String username) throws HelloException;

    /**
     * Update displayName for user.
     *
     * @param username    User name
     * @param displayName Name to display for this user
     * @throws HelloException If username is not a known user
     */
    void updateUser(String username, String displayName) throws HelloException;

    /**
     * Remove all users.
     */
    void removeAllUsers();

    /**
     * Get number of users.
     * @return Number of users
     */
    int getNumberOfUsers();

    /**
     * Get list of user names.
     * @return User name list
     */
    List<String> getUserNames();
}