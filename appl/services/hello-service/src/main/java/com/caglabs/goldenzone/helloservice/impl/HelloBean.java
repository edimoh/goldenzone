package com.caglabs.goldenzone.helloservice.impl;

import com.caglabs.goldenzone.helloservice.client.Hello;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUser;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUserDao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Simple test bean.
 */
@WebService
@Stateless
@Remote(Hello.class)
public class HelloBean implements Hello {
    private Logger logger = Logger.getLogger(HelloBean.class.getName());

    @Inject
    private GoldenzoneUserDao goldenzoneUserDao;

    @PersistenceContext(unitName = "GoldenzonePU")
    private EntityManager entityManager;

    @Override
    public String sayHello(String name) throws HelloException {
        initUserDao();
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(name);
        if (user != null) {
            String message = "Hello " + user.getDisplayName() + " (id=" + user.getId() + ")!!! The time is " +
                    new SimpleDateFormat("HH:mm:ss").format(new Date());
            logger.info(message);
            return message;
        } else {
            String message = "Unknown user: " + name;
            logger.info(message);
            throw new HelloException(message);
        }
    }

    @Override
    public String getDisplayName(String username) throws HelloException {
        initUserDao();
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(username);
        if (user != null) {
            return user.getDisplayName();
        }
        String message = "Unknown user: " + username;
        logger.info(message);
        throw new HelloException(message);
    }

    @Override
    public void addUser(String username, String displayName) throws HelloException {
        initUserDao();
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(username);
        if (user == null) {
            goldenzoneUserDao.save(new GoldenzoneUser(username, displayName));
        } else {
            String message = "addUser failed, username does already exist: " + username;
            logger.info(message);
            throw new HelloException(message);
        }
    }

    @Override
    public void removeUser(String username) throws HelloException {
        initUserDao();
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(username);
        if (user == null) {
            String message = "removeUser failed, username does not exist: " + username;
            logger.info(message);
            throw new HelloException(message);
        }
        goldenzoneUserDao.remove(user);
    }

    @Override
    public void updateUser(String username, String displayName) throws HelloException {
        initUserDao();
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(username);
        if (user == null) {
            String message = "updateUser failed, username does not exist: " + username;
            logger.info(message);
            throw new HelloException(message);
        }
        user.setDisplayName(displayName);
        entityManager.merge(user);
    }

    @Override
    public void removeAllUsers() {
        initUserDao();
        entityManager.createQuery("delete from GoldenzoneUser u").executeUpdate();
    }

    @Override
    public int getNumberOfUsers() {
        long count = entityManager.createQuery("select count (u.name) from GoldenzoneUser u", Long.class).getSingleResult();
        return (int) count;
    }

    @Override
    public List<String> getUserNames() {
        List<GoldenzoneUser> users = entityManager.createQuery("select su FROM GoldenzoneUser su").getResultList();
        List<String> result = new ArrayList<>();
        for (GoldenzoneUser user : users) {
            result.add(user.getName());
        }
        return result;
    }

    private void initUserDao() {
        if (goldenzoneUserDao.entityManager == null) {
            goldenzoneUserDao.entityManager = entityManager;
        }
    }
}
